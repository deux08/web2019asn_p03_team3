﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.DAL
{
    public class ProjectDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        public ProjectDAL()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("EPortfolioConnectionString");

            conn = new SqlConnection(strConn);
        }

        public List<Project> getAllProjects()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project ORDER BY ProjectID", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "ProjectDetails");
            conn.Close();

            List<Project> projectList = new List<Project>();

            foreach (DataRow row in result.Tables["ProjectDetails"].Rows)
            {
                projectList.Add(
                new Project
                {
                    ProjectID = Convert.ToInt32(row["ProjectID"].ToString()),
                    ProjectTitle = row["Title"].ToString(),
                    ProjectDescription = row["Description"].ToString(),
                    ProjectPoster = row["ProjectPoster"].ToString(),
                    ProjectURL = row["ProjectURL"].ToString()
                });
            }

            return projectList;
        }


        public List<Project> getProjectByStudent(int studentId)//get from session cookie
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM ProjectMember PM INNER JOIN Project P ON PM.ProjectID = P.ProjectID WHERE PM.StudentID = @selectedStudentId", conn);

            cmd.Parameters.AddWithValue("@selectedStudentId", studentId);
            

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "ProjectStudentDetails");
            conn.Close();

            List<Project> projectbyStudentList = new List<Project>();

            if (result.Tables["ProjectStudentDetails"].Rows.Count > 0)
            {
                foreach (DataRow row in result.Tables["ProjectStudentDetails"].Rows)
                {
                    projectbyStudentList.Add(
                    new Project
                    {
                        ProjectID = Convert.ToInt32(row["ProjectID"].ToString()),
                        ProjectTitle = row["Title"].ToString(),
                        ProjectDescription = row["Description"].ToString(),
                        ProjectPoster = row["ProjectPoster"].ToString(),
                        ProjectURL = row["ProjectURL"].ToString()
                    });
                }

                return projectbyStudentList; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }



        public Project GetProjectDetails(int projectId)
        {

            SqlCommand cmd = new SqlCommand("SELECT * FROM Project WHERE ProjectID = @selectedStaffID", conn);

            cmd.Parameters.AddWithValue("@selectedStaffID", projectId);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "ProjectDetails");
            conn.Close();


            Project project = new Project();
            if (result.Tables["ProjectDetails"].Rows.Count > 0)
            {
                project.ProjectID = projectId;

                DataTable table = result.Tables["ProjectDetails"];

                if (!DBNull.Value.Equals(table.Rows[0]["ProjectID"]))
                    project.ProjectID = Convert.ToInt32(table.Rows[0]["ProjectID"]);

                if (!DBNull.Value.Equals(table.Rows[0]["Title"]))
                    project.ProjectTitle = table.Rows[0]["Title"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                    project.ProjectDescription = table.Rows[0]["Description"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["ProjectPoster"]))
                    project.ProjectPoster = table.Rows[0]["ProjectPoster"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["ProjectURL"]))
                    project.ProjectURL = table.Rows[0]["ProjectURL"].ToString();


                return project; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }

        public int AddProject(Project project, int studentId)
        {
            SqlCommand cmd = new SqlCommand
            ("INSERT INTO Project (Title, Description, ProjectPoster, ProjectURL) " +
            "OUTPUT INSERTED.ProjectID " +
            "VALUES(@Title, @Description, @ProjectPoster, @ProjectURL)", conn);

            cmd.Parameters.AddWithValue("@Title", project.ProjectTitle);
            cmd.Parameters.AddWithValue("@Description", project.ProjectDescription);
            cmd.Parameters.AddWithValue("@ProjectPoster", project.ProjectPoster);
            cmd.Parameters.AddWithValue("@ProjectURL", project.ProjectURL);

            foreach (SqlParameter Parameter in cmd.Parameters)
            {
                if (Parameter.Value == null)
                {
                    Parameter.Value = DBNull.Value;
                }
            }

            conn.Open();
            project.ProjectID = (int)cmd.ExecuteScalar();

            SqlCommand cmd2 = new SqlCommand
                ("INSERT INTO ProjectMember(ProjectID, StudentID, Role)" +
                "VALUES(@ProjectID, @StudentID, @Role)", conn);

            cmd2.Parameters.AddWithValue("@ProjectID", Convert.ToInt32(project.ProjectID));
            cmd2.Parameters.AddWithValue("@StudentID", Convert.ToInt32(studentId));
            cmd2.Parameters.AddWithValue("@Role", "Leader");

            var result = cmd2.ExecuteNonQuery();
            conn.Close();

            return project.ProjectID; 
        }

        public int Update(Project project)
        {
            SqlCommand cmd = new SqlCommand
            ("UPDATE Project SET Title=@Title, Description=@Description, ProjectURL=@ProjectURL WHERE ProjectID=@selectedProjectId", conn);

            cmd.Parameters.AddWithValue("@Title", project.ProjectTitle);
            cmd.Parameters.AddWithValue("@Description", project.ProjectDescription);
            cmd.Parameters.AddWithValue("@ProjectURL", project.ProjectURL);
            cmd.Parameters.AddWithValue("@selectedProjectId", project.ProjectID);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            return count;
        }

        public int Delete(int projectId)
        {             
            SqlCommand cmd1 = new SqlCommand("DELETE FROM ProjectMember WHERE ProjectID=@selectedProjID", conn);
            SqlCommand cmd2 = new SqlCommand("DELETE FROM Project WHERE ProjectID=@selectedProjID", conn);

            cmd1.Parameters.AddWithValue("@selectedProjID", projectId);
            cmd2.Parameters.AddWithValue("@selectedProjID", projectId);

            int rowcount;

            conn.Open();
            rowcount = cmd1.ExecuteNonQuery();
            rowcount = cmd2.ExecuteNonQuery();
            conn.Close();

            return rowcount;
        }

        public void Upload(string fileName, int id)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Project SET ProjectPoster=@poster WHERE ProjectID=@selectedProjectId", conn);

            cmd.Parameters.AddWithValue("@poster", fileName);
            cmd.Parameters.AddWithValue("@selectedProjectId", id);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public List<Student> getMembers(int id)
        {
            SqlCommand cmd = new SqlCommand("SELECT S.* FROM Student S INNER JOIN ProjectMember PM ON S.StudentID = PM.StudentID WHERE PM.ProjectID = @id AND Role=@Role", conn);

            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@Role", "Member");


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "Members");
            conn.Close();

            List<Student> members = new List<Student>();

            if (result.Tables["Members"].Rows.Count > 0)
            {
                foreach (DataRow row in result.Tables["Members"].Rows)
                {
                    members.Add(
                    new Student
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        StudentName = row["Name"].ToString(),
                        StudentCourse = row["Course"].ToString(),
                        StudentPhoto = row["Photo"].ToString(),
                        StudentDescription = row["Description"].ToString(),
                        StudentAchievement = row["Achievement"].ToString(),
                        StudentEmailAddr = row["EmailAddr"].ToString(),
                        StudentPassword = row["Password"].ToString(),
                        MentorID = Convert.ToInt32(row["MentorID"]),
                        StudentLink = row["ExternalLink"].ToString(),
                    });
                }

                return members; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }

        public void addMembers(int studentId, int projectID)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO ProjectMember(ProjectID, StudentID, Role)" +
                                            "VALUES(@ProjectID, @StudentID, @Role)", conn);

            cmd.Parameters.AddWithValue("@StudentID", studentId);
            cmd.Parameters.AddWithValue("@ProjectID", projectID);
            cmd.Parameters.AddWithValue("@Role", "Member");
            
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void removeMembers(int studentId, int projectID)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM ProjectMember WHERE ProjectID=@selectedProjID AND StudentID=@StudentID AND Role=@Role", conn);

            cmd.Parameters.AddWithValue("@StudentID", studentId);
            cmd.Parameters.AddWithValue("@selectedProjID", projectID);
            cmd.Parameters.AddWithValue("@Role", "Member");

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public Student getLeader(int projectID)
        {
            SqlCommand cmd = new SqlCommand("SELECT S.* FROM Student S INNER JOIN ProjectMember PM ON S.StudentID = PM.StudentID WHERE PM.ProjectID = @ProjectID AND PM.Role = 'Leader'", conn);
            cmd.Parameters.AddWithValue("@ProjectID", projectID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "Leader");
            conn.Close();

            Student leader = new Student();

            if (result.Tables["Leader"].Rows.Count > 0)
            {
                foreach (DataRow row in result.Tables["Leader"].Rows)
                {
                    leader = 
                    new Student
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        StudentName = row["Name"].ToString(),
                        StudentCourse = row["Course"].ToString(),
                        StudentPhoto = row["Photo"].ToString(),
                        StudentDescription = row["Description"].ToString(),
                        StudentAchievement = row["Achievement"].ToString(),
                        StudentEmailAddr = row["EmailAddr"].ToString(),
                        StudentPassword = row["Password"].ToString(),
                        MentorID = Convert.ToInt32(row["MentorID"]),
                        StudentLink = row["ExternalLink"].ToString(),
                    };
                }

                return leader; // No error occurs
            }
            else
            {
                return null; // Record not found
            }


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.DAL
{
    public class SkillsetDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        public SkillsetDAL()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("EPortfolioConnectionString");

            conn = new SqlConnection(strConn);
        }

        public List<SkillSet> getAllSkillSets()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet ORDER BY SkillSetID", conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "SkillSetDetails");
            conn.Close();

            List<SkillSet> skillSetList = new List<SkillSet>();

            foreach (DataRow row in result.Tables["SkillSetDetails"].Rows)
            {
                skillSetList.Add(
                    new SkillSet
                    {
                        SkillSetID = Convert.ToInt32(row["SkillSetID"].ToString()),
                        SkillSetName = row["SkillSetName"].ToString()
                    });
            }
            return skillSetList;
        }

        public SkillSet GetSkillSetDetails(int ssid)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet WHERE SkillSetID = @selectedSkillSetID", conn);
            

            cmd.Parameters.AddWithValue("@selectedSkillSetID", ssid);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "SkillSetDetails");
            conn.Close();

            SkillSet ss = new SkillSet();
            if (result.Tables["SkillSetDetails"].Rows.Count > 0)
            {
                ss.SkillSetID = ssid;

                DataTable table = result.Tables["SkillSetDetails"];

                if (!DBNull.Value.Equals(table.Rows[0]["SkilLSetID"]))
                    ss.SkillSetID = Convert.ToInt32(table.Rows[0]["SkillSetID"]);

                if (!DBNull.Value.Equals(table.Rows[0]["SkillSetName"]))
                    ss.SkillSetName = table.Rows[0]["SkillSetName"].ToString();

                return ss; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }
        public List<SkillSet> Getallskillst()
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet", conn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "SkillSetDetails");
            conn.Close();

            List<SkillSet> allskillset = new List<SkillSet>();

            foreach (DataRow row in result.Tables["SkillSetDetails"].Rows)
            {
                allskillset.Add(
                    new SkillSet
                    {
                        SkillSetID = Convert.ToInt32(row["SkillSetID"]),
                        SkillSetName = row["SkillSetName"].ToString()
                    }
                    );

            }
            return allskillset;
        }

        public List<SkillSet> GetskillsetStudent(int studentid)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement     
            //which will return the auto-generated studentID after insertion,   
            //and the connection object for connecting to the database.  
            SqlCommand cmd = new SqlCommand(
             "select SK.* from SkillSet SK INNER JOIN StudentSkillSet SS ON SS.SkillSetID = SK.SkillSetID WHERE SS.StudentID = @selectedstudentID", conn);
            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            cmd.Parameters.AddWithValue("@selectedstudentID", studentid);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "SkillSetDetails");
            conn.Close();

            List<SkillSet> studentskillSetslist = new List<SkillSet>();
            
            foreach (DataRow row in result.Tables["SkillSetDetails"].Rows)
            {
                studentskillSetslist.Add(
                    new SkillSet
                    {
                        SkillSetID = Convert.ToInt32(row["SkillSetID"]),
                        SkillSetName = row ["SkillSetName"].ToString()
                    }
                    );

            }
            return studentskillSetslist;
        }

        public int AddSkillSet(SkillSet ss)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO SkillSet (SkillSetName) " + "OUTPUT INSERTED.SkillSetID " + "VALUES(@SkillSetName)", conn);
            SqlCommand cmd2 = new SqlCommand("SET IDENTITY_INSERT [dbo].[SkillSet] ON", conn);
            SqlCommand cmd3 = new SqlCommand("SET IDENTITY_INSERT [dbo].[SkillSet] OFF", conn);

            //cmd.Parameters.AddWithValue("@SkillSetID", ss.SkillSetID);
            cmd.Parameters.AddWithValue("@SkillSetName", ss.SkillSetName);

            conn.Open();
            //cmd2.ExecuteScalar();
            ss.SkillSetID = (int)cmd.ExecuteScalar();
            //cmd3.ExecuteScalar();
            conn.Close();

            return ss.SkillSetID;
        }

        public int Update(SkillSet ss)
        {
            SqlCommand cmd = new SqlCommand("UPDATE SkillSet SET SkillSetName=@SkillSetName" + " WHERE SkillSetID = @selectedSkillSetID", conn);

            //cmd.Parameters.AddWithValue("@SkillSetID", ss.SkillSetID);
            cmd.Parameters.AddWithValue("@selectedskillsetid", ss.SkillSetID);
            cmd.Parameters.AddWithValue("@SkillSetName", ss.SkillSetName);

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();

            return count;
        }

        public int Delete(int ssid)
        {
            SqlCommand cmd = new SqlCommand("DELETE FROM SkillSet WHERE SkillSetID = @selectedSkillSetID", conn);
            SqlCommand cmd2 = new SqlCommand("DELETE FROM StudentSkillSet WHERE SkillSetID = @selectedSkillSetID", conn);

            cmd.Parameters.AddWithValue("@selectedSkillSetID", ssid);
            cmd2.Parameters.AddWithValue("@selectedSKillSetID", ssid);

            int rowCount;

            conn.Open();
            rowCount = cmd.ExecuteNonQuery();
            rowCount = cmd2.ExecuteNonQuery();
            conn.Close();

            return rowCount;
        }
        
        public SkillSet GetDetails(int ssid)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Skillset WHERE SkillSetID = @selectedSkillSetID", conn);
            cmd.Parameters.AddWithValue("@selectedSkillSetID", ssid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet rs = new DataSet();

            conn.Open();
            da.Fill(rs, "SkillSetDetails");
            conn.Close();

            SkillSet ss = new SkillSet();
            if (rs.Tables["SkillSetDetails"].Rows.Count > 0)
            {
                ss.SkillSetID = ssid;
                DataTable table = rs.Tables["SkillSetDetails"];
                ss.SkillSetName = table.Rows[0]["SkillSetName"].ToString();

                return ss;
            }
            else
            {
                return null;
            }
        }

        public List<int> GetStudentID(int ssid)
        {
            SqlCommand cmd = new SqlCommand("SELECT StudentID FROM StudentSkillSet WHERE SkillSetID = @selectedSkillSetID", conn);
            cmd.Parameters.AddWithValue("@selectedSkillSetID", ssid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet rs = new DataSet();

            conn.Open();
            da.Fill(rs, "StudentIDDetails");
            conn.Close();

            List<int> studentIDList = new List<int>();
            foreach(DataRow d in rs.Tables["StudentIDDetails"].Rows)
            {
                studentIDList.Add(Convert.ToInt32(d["StudentID"]));
            }
            return studentIDList;
        }

        public bool isStudentSkillSetExists(int ssid)
        {
            SqlCommand cmd = new SqlCommand("SELECT StudentID FROM StudentSkillSet WHERE SkillSetID = @selectedSkillSetID", conn);
            cmd.Parameters.AddWithValue("@selectedSkillSetID", ssid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet rs = new DataSet();

            conn.Open();
            da.Fill(rs, "StudentSkillSetIDDetails");
            conn.Close();

            if(rs.Tables["StudentSkillSetIDDetails"].Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool isSkillSetExists(string ssn)
        {
            SqlCommand cmd = new SqlCommand("SELECT SkillSetName FROM SkillSet WHERE SkillSetName = @selectedSkillSetName", conn);
            cmd.Parameters.AddWithValue("@selectedSkillSetName", ssn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet rs = new DataSet();

            conn.Open();
            da.Fill(rs, "SkillSetNameDetails");
            conn.Close();

            if (rs.Tables["SkillSetNameDetails"].Rows.Count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
    }
}

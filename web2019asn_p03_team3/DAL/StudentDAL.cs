﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.DAL
{

    public class StudentDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        public StudentDAL()
        {
            //Constructor   
            //Locate the appsettings.json file   
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettings.json file  
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
                    "EPortfolioConnectionString");
            //Instantiate a SqlConnection object with the 
            //Connection String read.               
            conn = new SqlConnection(strConn);
        }
        public bool IsEmailExist(string email)
        {
            SqlCommand cmd = new SqlCommand
            ("SELECT StudentID FROM Student WHERE EmailAddr=@selectedEmail", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            //Use DataAdapter to fetch data to a table "EmailDetails" in DataSet.
            daEmail.Fill(result, "EmailDetails");
            conn.Close();
            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; //The email exists for another staff
            else
                return false; // The email address given does not exist
        }
        public List<Lecturer> getstudentlecturer()
        {
            SqlCommand cmd = new SqlCommand("select LecturerID, Name from Lecturer", conn);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            conn.Open();
            int count = cmd.ExecuteNonQuery();
            da.Fill(result, "LecturerDetails");
            conn.Close();
            List<Lecturer> lecturerlist = new List<Lecturer>();
            foreach (DataRow row in result.Tables["LecturerDetails"].Rows)
            {
                lecturerlist.Add(
                    new Lecturer
                    {
                        LecturerID = Convert.ToInt32(row["LecturerID"]),
                        LecturerName = row["Name"].ToString(),
                        
                    }
                );
            }
            return lecturerlist;
        }
        public List<Student> GetAllStudent()
        {
            //Instantiate a SqlCommand object, supply it with a
            //SELECT SQL statement that operates against the database,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand(
            "SELECT * FROM Student ORDER BY StudentID", conn);
            //Instantiate a DataAdapter object and pass the
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database
            DataSet result = new DataSet();
            //Open a database connection
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its
            //SqlCommand object to fetch data to a table "StudentfDetails"
            //in DataSet "result".
            da.Fill(result, "StudentDetails");
            //Close the database connection
            conn.Close();

            //Transferring rows of data in DataSet’s table to “Student” objects
            List<Student> studentList = new List<Student>();
            foreach (DataRow row in result.Tables["StudentDetails"].Rows)
            {
                studentList.Add(
                    new Student
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        StudentName = row["Name"].ToString(),
                        StudentCourse = row["Course"].ToString(),
                        StudentPhoto = row["Photo"].ToString(),
                        StudentDescription = row["Description"].ToString(),
                        StudentAchievement = row["Achievement"].ToString(),
                        StudentEmailAddr = row["EmailAddr"].ToString(),
                        StudentPassword = row["Password"].ToString(),
                        MentorID = Convert.ToInt32(row["MentorID"]),
                        StudentLink = row["ExternalLink"].ToString(),

                        
                    }
                );
            }
            return studentList;
           
        }
        public int Update(Student student)
        {
            SqlCommand cmd = new SqlCommand
            ("UPDATE Student SET Achievement=@Achievement, Description=@Description " +
             "WHERE StudentID=@selectedStudentID", conn);
            
            cmd.Parameters.AddWithValue("@selectedStudentID",student.StudentID);
            cmd.Parameters.AddWithValue("@Achievement", student.StudentAchievement);
            cmd.Parameters.AddWithValue("@Description", student.StudentDescription);
            

            //SqlDataAdapter da = new SqlDataAdapter(cmd2);
            //DataSet result = new DataSet();

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            //da.Fill(result);
            conn.Close();
            return student.StudentID;

        }

        

        public int updateskills(StudentViewModel student)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO StudentSkillSet(StudentID, SkillSetID)" +
                " VALUES(@selectedstudentid, @selectedskillid)", conn);

            cmd.Parameters.AddWithValue("@selectedstudentid", student.StudentID);
            cmd.Parameters.AddWithValue("@selectedskillid", student.StudentskillId);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            conn.Close();


            return student.StudentID;
        }


        public int Addrecords(Student student)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement     
            //which will return the auto-generated studentID after insertion,   
            //and the connection object for connecting to the database.  
            SqlCommand cmd = new SqlCommand
                ("INSERT INTO Student (Name, EmailAddr, Password,MentorID, Course) " +
                "OUTPUT INSERTED.StudentID " +
                "VALUES(@Name, @EmailAddr, @Password, @MentorID, @Course) "
                , conn);
            //Define the parameters used in SQL statement, value for each parameter   
            //is retrieved from respective class's property.   
            cmd.Parameters.AddWithValue("@Name", student.StudentName);
            cmd.Parameters.AddWithValue("@EmailAddr", student.StudentEmailAddr);
            cmd.Parameters.AddWithValue("@Password", student.StudentPassword);
            cmd.Parameters.AddWithValue("@Course", " ");
            //cmd.Parameters.AddWithValue("@Description", student.StudentDescription);
            cmd.Parameters.AddWithValue("@MentorID", student.MentorID);
            
            //A connection to database must be opened before any operations made. 
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated    
            //StaffID after executing the INSERT SQL statement  
            student.StudentID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.  
            conn.Close();

            //Return id when no error occurs.   
            return student.StudentID;
        }
        //uploading the picture
        public void Upload(string fileName, int studentid)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Photo=@selectedphoto WHERE StudentID = @selectedStudentId", conn);

            cmd.Parameters.AddWithValue("@selectedphoto", fileName);
            cmd.Parameters.AddWithValue("@selectedStudentId", studentid);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public Student GetStudentProfile(int studentId)
        {

            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE StudentID = @selectedStaffID", conn);

            cmd.Parameters.AddWithValue("@selectedStaffID", studentId);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();

            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            da.Fill(result, "StudentDetails");

            //Close the database connection
            conn.Close();

            Student student = new Student();
            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                student.StudentID = studentId;

                DataTable table = result.Tables["StudentDetails"];

                //if (!DBNull.Value.Equals(table.Rows[0]["StudentID"]))
                  //  student.StudentID = Convert.ToInt32(table.Rows[0]["StudentID"]);

                if (!DBNull.Value.Equals(table.Rows[0]["MentorID"]))
                    student.MentorID = Convert.ToInt32(table.Rows[0]["MentorID"]);

                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                    student.StudentName = table.Rows[0]["Name"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["Course"]))
                    student.StudentCourse = table.Rows[0]["Course"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                    student.StudentDescription = table.Rows[0]["Description"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                    student.StudentAchievement = table.Rows[0]["Achievement"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["Photo"]))
                    student.StudentPhoto = table.Rows[0]["Photo"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                    student.StudentEmailAddr = table.Rows[0]["EmailAddr"].ToString();

                if (!DBNull.Value.Equals(table.Rows[0]["ExternalLink"]))
                    student.StudentLink = table.Rows[0]["ExternalLink"].ToString();

               

                return student; // No error occurs
            }
            else
            {
                return null; // Record not found
            }
        }
        //Adel student suggestion list
        public List<Suggestion> GetStudentSuggestions(int studentid)
        {

            SqlCommand scmd = new SqlCommand(
                "select * from Suggestion " +
                "where StudentID = @userId", conn);

            scmd.Parameters.AddWithValue("@userId", studentid);

            SqlDataAdapter data = new SqlDataAdapter(scmd);

            DataSet result = new DataSet();

            conn.Open();

            data.Fill(result, "studentdetails");

            conn.Close();

            //Transferring rows of data in DataSet’s table to “Suggestion” objects    
            List<Suggestion> studentsuggestionList = new List<Suggestion>();
            foreach (DataRow row in result.Tables["studentdetails"].Rows)
            {
                studentsuggestionList.Add(
                    new Suggestion
                    {
                        SuggestionID = Convert.ToInt32(row["SuggestionID"]),
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        DateCreated = Convert.ToDateTime(row["DateCreated"]),
                        SuggestionDescription = Convert.ToString(row["Description"]),
                        SuggestionStatus = Convert.ToChar(row["Status"]),
                        LecturerID = Convert.ToInt32(row["LecturerID"]),
                    }
                );
            }
            return studentsuggestionList;
        }
        public int Acknowledge(int sId, int studentId)
        {
            SqlCommand cmd = new SqlCommand
            ("Update Suggestion set Status = 'Y'" +
            "where StudentID =@studentid and SuggestionID = @suggestionid", conn);

            cmd.Parameters.AddWithValue("@studentid", studentId);
            cmd.Parameters.AddWithValue("@suggestionid", sId);
            
            //SqlDataAdapter da = new SqlDataAdapter(cmd2);
            //DataSet result = new DataSet();

            conn.Open();
            int count = cmd.ExecuteNonQuery();
            //da.Fill(result);
            conn.Close();
            return studentId;

        }
    }
}

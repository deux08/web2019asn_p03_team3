﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.DAL
{
    public class LecturerDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor   
        public LecturerDAL()
        {
            //Locate the appsettings.json file   
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettings.json file  
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
                    "EPortfolioConnectionString");
            //Instantiate a SqlConnection object with the 
            //Connection String read.               
            conn = new SqlConnection(strConn);
        }

        public List<Lecturer> GetAllLecturer()
        {
            //Instantiate a SqlCommand object, supply it with a         
            //SELECT SQL statement that operates against the database,    
            //and the connection object for connecting to the database.      
            SqlCommand cmd = new SqlCommand(
                "SELECT * FROM Lecturer ORDER BY LectuerID", conn);
            //Instantiate a DataAdapter object and pass the          
            //SqlCommand object created as parameter.
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //Create a DataSet object to contain records get from database   
            DataSet result = new DataSet();

            //Open a database connection   
            conn.Open();
            //Use DataAdapter, which execute the SELECT SQL through its    
            //SqlCommand object to fetch data to a table "LecturerDetails"   
            //in DataSet "result".            
            da.Fill(result, "LecturerDetails");
            //Close the database connection               
            conn.Close();

            //Transferring rows of data in DataSet’s table to “Lecturer” objects    
            List<Lecturer> lecturerList = new List<Lecturer>();
            foreach (DataRow row in result.Tables["LecturerDetails"].Rows)
            {
                lecturerList.Add(
                    new Lecturer
                    {
                        LecturerID = Convert.ToInt32(row["LecturerID"]),
                        LecturerName = row["Name"].ToString(),
                        LecturerEmailAddr = row["EmailAddr"].ToString(),
                        LecturerPassword = row["Password"].ToString(),
                        LecturerDescription = row["Description"].ToString(),
                    }
                );
            }
            return lecturerList;
        }
        public int Add(Lecturer lecturer)
        {
            //Instantiate a SqlCommand object,supply it with an INSERT SQL statement     
            //which will return the auto-generated LecturerID after insertion,   
            //and the connection object for connecting to the database.  
            SqlCommand cmd = new SqlCommand
                ("INSERT INTO Lecturer (Name, EmailAddr, Password, Description) " +
                "OUTPUT INSERTED.LecturerID " +
                "VALUES(@Name, @EmailAddr, @Password, @Description) "
                , conn);
            //Define the parameters used in SQL statement, value for each parameter   
            //is retrieved from respective class's property.   
            cmd.Parameters.AddWithValue("@Name", lecturer.LecturerName);
            cmd.Parameters.AddWithValue("@EmailAddr", lecturer.LecturerEmailAddr);
            cmd.Parameters.AddWithValue("@Password", lecturer.LecturerPassword);
            cmd.Parameters.AddWithValue("@Description", lecturer.LecturerDescription);

            //A connection to database must be opened before any operations made. 
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated    
            //StaffID after executing the INSERT SQL statement  
            lecturer.LecturerID = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.  
            conn.Close();

            //Return id when no error occurs.   
            return lecturer.LecturerID;
        }

        public bool IsEmailExist(string email)
        {
            SqlCommand cmd = new SqlCommand
            ("SELECT LecturerID FROM Lecturer WHERE EmailAddr=@selectedEmail", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            SqlDataAdapter lecemail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            //Use DataAdapter to fetch data to a table "EmailDetails" in DataSet.
            lecemail.Fill(result, "EmailDetails");
            conn.Close();
            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; //The email exists for another staff
            else
                return false; // The email address given does not exist
        }

        public bool CheckStudentID (int StudentID)
        {
            SqlCommand cmd = new SqlCommand("SELECT StudentID FROM Student WHERE StudentID = @sid", conn);
            cmd.Parameters.AddWithValue("@sid", StudentID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "StudentDetails");
            conn.Close();

            int dbStudentID = 0;
            foreach (DataRow row in result.Tables["StudentDetails"].Rows)
            {
                dbStudentID = Convert.ToInt32(row["StudentID"]);
            }

            if (dbStudentID == StudentID)
            {
                return true;
            }

            else
            {
                return false;
            }
    }

        public int GetLecturerID(string loginID)
        {
            SqlCommand cmd = new SqlCommand("SELECT LecturerID FROM Lecturer WHERE EmailAddr = @email", conn);
            cmd.Parameters.AddWithValue("@email", loginID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "LecturerDetails");
            conn.Close();

            int MentorID = 0;

            foreach(DataRow row in result.Tables["LecturerDetails"].Rows)
            {
                MentorID = Convert.ToInt32(row["LecturerID"]);
            }

            return MentorID;
        }

        public List<Student> GetAllMentees(int MentorID)
        {
            SqlCommand cmd = new SqlCommand(
             "SELECT * FROM Student WHERE MentorID = @mentorid", conn);

            cmd.Parameters.AddWithValue("@mentorid", MentorID);

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();
            da.Fill(result, "StudentDetails");
            conn.Close();

            List<Student> studentList = new List<Student>();
            foreach (DataRow row in result.Tables["StudentDetails"].Rows)
            {
                studentList.Add(
                    new Student
                    {
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        StudentName = row["Name"].ToString(),
                        StudentCourse = row["Course"].ToString(),
                        StudentPhoto = row["Photo"].ToString(),
                        StudentDescription = row["Description"].ToString(),
                        StudentAchievement = row["Achievement"].ToString(),
                        StudentLink = row["ExternalLink"].ToString(),
                        StudentEmailAddr = row["EmailAddr"].ToString(),
                        StudentPassword = row["Password"].ToString(),
                        MentorID = Convert.ToInt32(row["MentorID"])
                    }
                    );
                
            }
            return studentList;
        }

        public bool accountChecker(string email, string password)
        {
            SqlCommand cmd = new SqlCommand
                ("SELECT LecturerID, Name FROM Lecturer WHERE EmailAddr=@selectedEmail AND Password = @selectedPwd", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            cmd.Parameters.AddWithValue("@selectedPwd", password);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            da.Fill(result, "AccountDetails");
            conn.Close();

            if (result.Tables["AccountDetails"].Rows.Count == 1)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public bool changePassword(string email, string newpassword)
        {
            SqlCommand cmd = new SqlCommand("UPDATE Lecturer SET Password = @pwd WHERE EmailAddr = @loginid", conn);

            cmd.Parameters.AddWithValue("@pwd", newpassword);
            cmd.Parameters.AddWithValue("@loginid", email);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return true;
        }

        public Student GetMentee (int sid)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE StudentID = @selectedStudentID", conn);
            cmd.Parameters.AddWithValue("@selectedStudentID", sid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet rs = new DataSet();

            conn.Open();
            da.Fill(rs, "StudentDetails");
            conn.Close();

            Student student = new Student();
            if (rs.Tables["StudentDetails"].Rows.Count > 0)
            {
                student.StudentID = sid;
                DataTable table = rs.Tables["StudentDetails"];
                student.StudentID = Convert.ToInt32(table.Rows[0]["StudentID"]);
                student.StudentName = table.Rows[0]["Name"].ToString();
                student.StudentCourse = table.Rows[0]["Course"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Photo"]))
                    student.StudentPhoto = table.Rows[0]["Photo"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                    student.StudentDescription = table.Rows[0]["Description"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                    student.StudentAchievement = table.Rows[0]["Achievement"].ToString();
                if (!DBNull.Value.Equals(table.Rows[0]["ExternalLink"]))
                    student.StudentLink = table.Rows[0]["ExternalLink"].ToString();
                return student;
            }
            else
            {
                return null;
            }
        }

        public List<SkillSet> GetSkillSets(int sid)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet s INNER JOIN StudentSkillSet sss ON s.SkillSetID = sss.SkillSetID WHERE StudentID = @selectedStudentID", conn);
            cmd.Parameters.AddWithValue("@selectedStudentID", sid);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet rs = new DataSet();

            conn.Open();
            da.Fill(rs, "MenteeSkillSets");
            conn.Close();

            List<SkillSet> skillSetList = new List<SkillSet>();
            if (rs.Tables["MenteeSkillSets"].Rows.Count > 0)
            {
                foreach (DataRow row in rs.Tables["MenteeSkillSets"].Rows)
                {
                    skillSetList.Add(new SkillSet
                    {
                        SkillSetName = row["SkillSetName"].ToString(),
                    });
                }
                return skillSetList;
            }
            else
            {
                return null;
            }
        }

    }
}


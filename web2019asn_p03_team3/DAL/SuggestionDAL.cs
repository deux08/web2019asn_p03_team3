﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.DAL
{
    public class SuggestionDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        //Constructor   
        public SuggestionDAL()
        {
            //Locate the appsettings.json file   
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettings.json file  
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
                    "EPortfolioConnectionString");
            //Instantiate a SqlConnection object with the 
            //Connection String read.               
            conn = new SqlConnection(strConn);
        }
        public List<Suggestion> GetAllSuggestions(int lecturerID)
        {
     
            SqlCommand scmd = new SqlCommand(
                "SELECT * FROM Suggestion WHERE LecturerID = @userId Order By DateCreated", conn);

            scmd.Parameters.AddWithValue("@userId", lecturerID);

            SqlDataAdapter data = new SqlDataAdapter(scmd);

            DataSet result = new DataSet();

            conn.Open();
            
            data.Fill(result, "SuggestionDetails");
             
            conn.Close();

            //Transferring rows of data in DataSet’s table to “Suggestion” objects    
            List<Suggestion> suggestionList = new List<Suggestion>();
            foreach (DataRow row in result.Tables["SuggestionDetails"].Rows)
            {
                suggestionList.Add(
                    new Suggestion
                    {
                        SuggestionID = Convert.ToInt32(row["SuggestionID"]),
                        StudentID = Convert.ToInt32(row["StudentID"]),
                        DateCreated = Convert.ToDateTime(row["DateCreated"]),
                        SuggestionDescription = Convert.ToString(row["Description"]),
                        SuggestionStatus = Convert.ToChar(row["Status"]),
                        LecturerID = Convert.ToInt32(row["LecturerID"]),
                    }
                );
            }
            return suggestionList;
        }

       


        public int AddSuggestion(int StudentID, int LecturerID, string suggestionDescription)
        {

            SqlCommand cmd = new SqlCommand
              ("INSERT INTO Suggestion (LecturerID, StudentID, Description, Status ) " +
              "OUTPUT INSERTED.SuggestionID " +
              "VALUES(@LecturerID, @StudentID, @Description, @Status ) "
              , conn);

            //Define the parameters used in SQL statement, value for each parameter   
            //is retrieved from respective class's property.   
            cmd.Parameters.AddWithValue("@StudentID", StudentID);
            cmd.Parameters.AddWithValue("@LecturerID", LecturerID);
            cmd.Parameters.AddWithValue("@Description", suggestionDescription);
            cmd.Parameters.AddWithValue("@Status", "N");

            conn.Open();
            int SuggestionID = (int)cmd.ExecuteScalar();
            conn.Close();

            return SuggestionID;

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.DAL
{
    public class LoginDAL
    {
        private IConfiguration Configuration { get; set; }
        private SqlConnection conn;
        public LoginDAL()
        {
            //Locate the appsettings.json file
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");

            //Read ConnectionString from appsettings.json file
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString("EPortfolioConnectionString");

            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public Student GetStudent(string email, string password)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE EmailAddr=@selectedEmail AND password=@selectedPassword ", conn);

            cmd.Parameters.AddWithValue("@selectedEmail", email);
            cmd.Parameters.AddWithValue("@selectedPassword", password);

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "StudentLoginDetails");

            conn.Close();

            Student student = new Student();

            if (result.Tables["StudentLoginDetails"].Rows.Count > 0)
            {
                DataTable table = result.Tables["StudentLoginDetails"];


                student.StudentID = Convert.ToInt32(table.Rows[0]["StudentID"]);
                student.StudentName = table.Rows[0]["Name"].ToString();

                return student;
            }
            else
            {
                return null;
            }
        }

        public Lecturer GetLecturer(string email, string password)
        {
            SqlCommand cmd = new SqlCommand("SELECT * FROM Lecturer WHERE EmailAddr=@selectedEmail AND password=@selectedPassword ", conn);

            cmd.Parameters.AddWithValue("@selectedEmail", email);
            cmd.Parameters.AddWithValue("@selectedPassword", password);

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet result = new DataSet();

            conn.Open();

            da.Fill(result, "LecturerLoginDetails");

            conn.Close();

            Lecturer lecturer = new Lecturer();

            if (result.Tables["LecturerLoginDetails"].Rows.Count > 0)
            {
                DataTable table = result.Tables["LecturerLoginDetails"];


                lecturer.LecturerID = Convert.ToInt32(table.Rows[0]["LecturerID"]);
                lecturer.LecturerName = table.Rows[0]["Name"].ToString();

                return lecturer;
            }
            else
            {
                return null;
            }
        }

        public bool LecturerEmailExist(string email)
        {
            SqlCommand cmd = new SqlCommand
                ("SELECT LecturerID FROM Lectuer WHERE EmailAddr=@selectedEmail", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            SqlDataAdapter daEmail = new SqlDataAdapter(cmd); DataSet result = new DataSet();
            conn.Open();     //Use DataAdapter to fetch data to a table "EmailDetails" in DataSet.    
            daEmail.Fill(result, "EmailDetails");
            conn.Close();
            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; //The email exists for another lecturer
            else
                return false; // The email address given does not exist 
        }
    }
}

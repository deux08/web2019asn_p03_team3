﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using web2019asn_p03_team3.DAL;
namespace web2019asn_p03_team3.Models
{
    
    public class ValidateEmailExistStudent : ValidationAttribute
    {
        private StudentDAL studentcontext = new StudentDAL();

        public override bool IsValid(object value)
        {
            string email = Convert.ToString(value);
            if (studentcontext.IsEmailExist(email))
            {
                return false; // validation failed
            }

            else
            {
                return true; // validation passed 
            }
                
        }
    }
}

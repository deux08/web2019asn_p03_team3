﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web2019asn_p03_team3.Models
{
    public class MenteeViewModel
    {
        public Student student { get; set; }
        public List<SkillSet> skillset { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web2019asn_p03_team3.Models
{
    public class SkillSet
    {
        public int SkillSetID { get; set; }

        public string SkillSetName { get; set; }
    }
}

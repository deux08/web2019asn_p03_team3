﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web2019asn_p03_team3.Models
{
    public class Lecturer
    {
        public int LecturerID { get; set; }

        [Display(Name = "Lecturer Name")]
        [Required(ErrorMessage = "Please enter a name")]
        [StringLength(50)]
        public string LecturerName { get; set; }

        [Display(Name = "Email")]
        [StringLength(50)]
        [Required(ErrorMessage = "Please enter staff email")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@ap.edu.sg")] //staff email (@ap.edu.sg)
        // Custom Validation Attribute for checking email address exists
        //[ValidateEmailExists(ErrorMessage = "Email address already exists!")] 
        public string LecturerEmailAddr { get; set; }

        [StringLength(255)]
        public string LecturerPassword { get; set; }

        [StringLength(3000)]
        [Display(Name = "Lecturer Description")]
        [Required(ErrorMessage = "Please enter a description.")]
        public string LecturerDescription { get; set; }
    }
}

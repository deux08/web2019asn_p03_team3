﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web2019asn_p03_team3.Models
{
    public class StudentSkillSet
    {
        public SkillSet SkillSet { get; set; }
        public List<Student> Students { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using web2019asn_p03_team3.DAL;

namespace web2019asn_p03_team3.Models
{
    public class ValidateEmailExists : ValidationAttribute
    {
        private LecturerDAL lecturerContext = new LecturerDAL();

        public override bool IsValid(object value)
        {
            string lectureremail = Convert.ToString(value);
            if (lecturerContext.IsEmailExist(lectureremail))
                return false; // validation failed
            else
                return true; // validation passed
        }

    }
}

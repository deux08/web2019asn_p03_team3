﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace web2019asn_p03_team3.Models
{
    public class Student
    {
        public int StudentID { get; set; }
        public int MentorID { get; set; }
        public int SkillID { get; set; }

        [Display(Name = "Student Name")]
        [Required(ErrorMessage = "Please enter a Name")]
        [StringLength(255)]
        public string StudentName { get; set; }

        [Display(Name = "Student Course")]
        //[Required(ErrorMessage = "Please enter a Course")]
        [StringLength(5)]
        public string StudentCourse { get; set; }

        public string StudentPhoto { get; set; }

        [Display(Name = "Student Description")]
        [StringLength(3000)]
        public string StudentDescription { get; set; }

        [Display(Name = "Student Achievement ")]
        [StringLength(3000)]
        public string StudentAchievement { get; set; }

        [Display(Name ="Student Email Address")]
        [StringLength(50)]
        [Required(ErrorMessage = "Please enter your email ")]
        [ValidateEmailExistStudent(ErrorMessage = "Email address already exists!")]
        [RegularExpression(@"[A-Za-z0-9._%+-]+@ap.edu.sg")]
        public string StudentEmailAddr { get; set; }

        [Display(Name = "External Link")]
        public string StudentLink { get; set; }

        [Display(Name = "Student Password")]
        [StringLength(255)]
        public string StudentPassword { get; set; }
        
        
    }
}

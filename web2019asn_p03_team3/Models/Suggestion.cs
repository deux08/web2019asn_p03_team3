﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace web2019asn_p03_team3.Models
{
    public class Suggestion
    {
        [Required]
        public int SuggestionID { get; set; }

        [Required]
        public int LecturerID { get; set; }

        [Required(ErrorMessage = "Please select a StudentID")]
        public int StudentID { get; set; }

        [StringLength(3000)] //able to accept null values
        public string SuggestionDescription { get; set; }

        [Required]
        public char SuggestionStatus { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web2019asn_p03_team3.Models
{
    public class StudentViewModel
    {
        public int StudentID { get; set; }
        public string MentorID { get; set; }

        [Display(Name = "Student Name")]
        public string StudentName { get; set; }

        [Display(Name = "Student Description")]
        public string StudentDescription { get; set; }

        [Display(Name = "Student Achievement")]
        public string StudentAchievement { get; set; }

        [Display(Name = "Student Picture")]
        public string StudentPhoto { get; set; }

        [Display(Name = "Student link ")]
        public string StudentLink { get; set; }

        [Display(Name ="Student EmailAddress")]
        public string StudentEmailAddr { get; set; }

        [Display(Name = "Student Course")]
        public string StudentCourse { get; set; }

        [Display(Name = "Chosen Skill")]
        public int StudentskillId { get; set; }

        [Display(Name = "List of student skills")]
        public List<SkillSet> Studentskills { get; set; }

        [Display(Name = "Skillsets")]
        public List<SkillSet> AllSkillSets { get; set; }

        [Display(Name = "Suggestion")]
        public List<Suggestion> StudentSuggestions{ get; set; }

        public IFormFile FileToUpload { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web2019asn_p03_team3.Models
{
    public class Project
    {
        //auto generated and is required, find a way to auto generate
        public int ProjectID { get; set; }

        [Display(Name = "Project Title")]
        [Required(ErrorMessage ="You must enter a project title")]
        [StringLength(255, ErrorMessage = "Maximum number of characters must be 255")]
        public string ProjectTitle { get; set; }

        [Display(Name = "Project Description")]
        [StringLength(3000, ErrorMessage = "Maximum number of characters must be 3000")]
        public string ProjectDescription { get; set; }

        [Display(Name = "Project Poster")]
        [StringLength(255, ErrorMessage = "Maximum number of characters must be 255")]
        public string ProjectPoster { get; set; }

        [Display(Name = "Project Url")]
        [StringLength(255, ErrorMessage = "Maximum number of characters must be 255")]
        public string ProjectURL { get; set; }


    }
}

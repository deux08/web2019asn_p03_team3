﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace web2019asn_p03_team3.Models
{
    public class ProjectMember
    {
        public int ProjectID { get; set; }

        public int StudentID { get; set; }

        public string StudentRole { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web2019asn_p03_team3.Models
{
    public class ProjectViewModel
    {
        public int ProjectID { get; set; }

        [Display(Name = "Project Title")]
        public string ProjectTitle { get; set; }

        [Display(Name = "Project Description")]
        public string ProjectDescription { get; set; }

        [Display(Name = "Project Poster")]
        public string ProjectPoster { get; set; }

        [Display(Name = "Project Url")]
        public string ProjectURL { get; set; }

        public IFormFile FileToUpload { get; set; }

        public List<Student> Members { get; set; }

        public Student Leader { get; set; }
    }
}

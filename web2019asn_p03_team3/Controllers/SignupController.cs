﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web2019asn_p03_team3.DAL;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.Controllers
{
    public class SignupController : Controller
    {
        private LecturerDAL lecturerContext = new LecturerDAL();

        public IActionResult LecturerSignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LecturerSignUp(Lecturer lecturer)
        {
            lecturer.LecturerPassword = "p@55Lecturer";
           if (ModelState.IsValid)
            {
                bool emailExist = lecturerContext.IsEmailExist(lecturer.LecturerEmailAddr);
                if (emailExist == false)
                {
                    // Add staff record to database  
                    lecturer.LecturerID = lecturerContext.Add(lecturer);
                    //Redirect user to Staff/Index view   
                    TempData["Message"] = "You have signed up successfully! Please login with default password.";
                    return Redirect("~/Login/LecturerLogin");
                }

                else
                {
                    TempData["Message"] = "Email address has already signed up, please log in.";
                    return Redirect("~/Login/LecturerLogin");
                }

            }
           else
            {
                //Input validation fails, return to the Create view to display error message                
                return View(lecturer);
           }
        }

        //adel mentor list
        private List<SelectListItem> GetLecturerLists()
        {
            List<SelectListItem> lecturers = new List<SelectListItem>();
            lecturers.Add(
            new SelectListItem
            {
                Value = "",
                Text = "--Select--"
            });
            List<Lecturer> lecturerlist = studentContext.getstudentlecturer();
            foreach (Lecturer lect in lecturerlist)
            {
                lecturers.Add(
                new SelectListItem
                {
                    Value = lect.LecturerID.ToString(),
                    Text = lect.LecturerName
                });
            }
            return lecturers;
        }

        private StudentDAL studentContext = new StudentDAL();


        public ActionResult StudentSignUp(Student student)
        {
            ViewData["Mentorlist"] = GetLecturerLists();
            student.StudentPassword = "p@55Student";
            if (ModelState.IsValid)
            {
               
                // Add Student record to database  
                student.StudentID = studentContext.Addrecords(student);
                //Redirect user to Student/signup view   
                TempData["Message"] = "You have signed up successfully! Please login with deafult password";
                return Redirect("~/Login/StudentLogin");
            }
            else
            {
                //Input validation fails, return to the Create view to display error message                
                return View(student);
            }
        }
    }
}
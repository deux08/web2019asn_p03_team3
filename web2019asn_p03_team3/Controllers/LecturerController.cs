﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web2019asn_p03_team3.DAL; 
using web2019asn_p03_team3.Models;


namespace web2019asn_p03_team3.Controllers
{
    public class LecturerController : Controller
    {
        private LecturerDAL lecturerContext = new LecturerDAL();

        // GET: Lecturer 
        public IActionResult Index()
        {
            //Stop accessing the action if not logged in or account not in the "Lecturer" role   
            if ((HttpContext.Session.GetString("Role") == null) ||
                (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                TempData["Message"] = "You must be logged in to access this page.";
                return RedirectToAction("Index", "Home");
            }

            List<Lecturer> lecturerList = lecturerContext.GetAllLecturer();
            return View(lecturerList);
        }
         //GET: Lecturer/Details/5
        public ActionResult Details(int LecturerID)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                TempData["Message"] = "You must be logged in to access this page.";
                return RedirectToAction("Index", "LecturerLogin");
            }
            return View();
        }

        public IActionResult ViewMentees()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                TempData["Message"] = "You must be logged in to access this page.";
                return RedirectToAction("Index", "LecturerLogin");
            }
            int LecturerID = lecturerContext.GetLecturerID(HttpContext.Session.GetString("LoginID")); //Get MentorID
            List<Student> studentList = lecturerContext.GetAllMentees(LecturerID);
            return View(studentList);
        }

        public IActionResult ChangePassword()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                TempData["Message"] = "You must be logged in to access this page.";
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpPost]
        public ActionResult PasswordChange(IFormCollection formData)
        {
            string oldpassword = formData["txtOldPwd"].ToString();
            string newpassword = formData["txtnewPwd"].ToString();
            string cfmpassword = formData["txtCfmPwd"].ToString();
            string email = HttpContext.Session.GetString("LoginID");

            bool correctOldPwd = lecturerContext.accountChecker(email, oldpassword); //Verify old password

            if (correctOldPwd == true)
            {
                if (newpassword == cfmpassword)
                {
                    //Check for 1 numeric char in password
                    bool meetsReq = false;
                    bool result = false;
                    int count = 0;
                    foreach (char c in newpassword)
                    {
                        result = false;
                        result = Char.IsDigit(c);
                        count += 1;

                        if (result == true)
                        {
                            meetsReq = true;
                        }
                    }

                    if (meetsReq == true && count >= 8)
                    {
                        bool updated = lecturerContext.changePassword(email, newpassword);

                        if (updated == true)
                        {
                            TempData["Message"] = "You have sucessfully changed your password.";
                            return Redirect("~/Lecturer/ChangePassword");
                        }

                        else
                        {
                            TempData["Message"] = "An unknown error occured.";
                            return Redirect("~/Lecturer/ChangePassword");
                        }
                    }

                    else
                    {
                        TempData["Message"] = "Your password must contain at least 1 digit and be at least 8 characters long.";
                        return Redirect("~/Lecturer/ChangePassword");
                    }

                }

                else
                {
                    TempData["Message"] = "New passwords do not match. No changes were made.";
                    return Redirect("~/Lecturer/ChangePassword");
                }
            }

            else
            {
                TempData["Message"] = "Incorrect old password. No changes were made.";
                return Redirect("~/Lecturer/ChangePassword");
            }
        }
    }
}
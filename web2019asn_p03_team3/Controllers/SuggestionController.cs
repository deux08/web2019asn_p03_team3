﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using web2019asn_p03_team3.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using web2019asn_p03_team3.DAL;

namespace web2019asn_p03_team3.Controllers
{
    public class SuggestionController : Controller
    {
        private SuggestionDAL suggestionContext = new SuggestionDAL();
        private LecturerDAL lecturerContext = new LecturerDAL();

        // GET: Suggestion
        public IActionResult Index(Suggestion suggestion)
        {
            if (HttpContext.Session.GetString("Role") == "Lecturer")
            {

                var lecturerId = HttpContext.Session.GetString("LecturerID");
                List<Suggestion> suggestionbyLecturerList = suggestionContext.GetAllSuggestions(Convert.ToInt32(lecturerId));
                return View(suggestionbyLecturerList);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        // GET: Suggestion/Create
        public ActionResult Create()
        {
            // Stop accessing the action if not logged in       
            // or account not in the "Lecturer" role           
            if ((HttpContext.Session.GetString("Role") != "Lecturer") || (HttpContext.Session.GetString("Role") == null))
            {
                return RedirectToAction("Index", "Home");
            }

            // Create List of student IDs for Create view
            int LecturerID = lecturerContext.GetLecturerID(HttpContext.Session.GetString("LoginID")); //Get MentorID
            List<Student> studentList = lecturerContext.GetAllMentees(LecturerID);
            List<int> studentIdList = new List<int>();
            foreach (Student student in studentList)
            {
                studentIdList.Add(student.StudentID);
            }
            ViewData["StudentList"] = studentIdList;

            Suggestion suggestion = new Suggestion();
            return View(suggestion);
        }

        // POST: Suggestion/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Suggestion suggestion)
        {
            // Stop accessing the action if not logged in       
            // or account not in the "Lecturer" role           
            if ((HttpContext.Session.GetString("Role") != "Lecturer") || (HttpContext.Session.GetString("Role") == null))
            {
                return RedirectToAction("Index", "Home");
            }

            int LecturerID = lecturerContext.GetLecturerID(HttpContext.Session.GetString("LoginID")); //Get MentorID
            int StudentID = Convert.ToInt32(suggestion.StudentID);
            string Description = suggestion.SuggestionDescription.ToString();
            // Add suggestion record to database  
            int SuggestionID = suggestionContext.AddSuggestion(StudentID, Convert.ToInt32(LecturerID), Description);
            List<Student> studentList = lecturerContext.GetAllMentees(LecturerID);
            List<int> studentIdList = new List<int>();
            foreach (Student student in studentList)
            {
                studentIdList.Add(student.StudentID);
            }

            ViewData["StudentList"] = studentIdList;
            TempData["message1"] = "You have posted your suggestion successfully!";

            return RedirectToAction("Index", "Suggestion");
            //return View(suggestion);
        }
    }
}
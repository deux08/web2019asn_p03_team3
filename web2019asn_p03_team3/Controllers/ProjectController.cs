﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web2019asn_p03_team3.DAL;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.Controllers
{
    public class ProjectController : Controller
    {
        private ProjectDAL projectContext = new ProjectDAL();
        private StudentDAL studentContext = new StudentDAL();
        // GET: Project
        public ActionResult Index()
        {
            if (HttpContext.Session.GetString("Role") == "Student")
            {
                var studentId = HttpContext.Session.GetString("StudentID");
                List<Project> projectbyStudentList = projectContext.getProjectByStudent(Convert.ToInt32(studentId));
                return View(projectbyStudentList);
            }
            else
            {
                List<Project> projectList = projectContext.getAllProjects();
                return View(projectList);
            }

            
        }

        // GET: Project/Details/5
        public ActionResult Details(int id)
        {
            Project project = projectContext.GetProjectDetails(id);
            ProjectViewModel ProjVM = MapToProjVM(project);
            //if (ProjVM.Members.Count > 1)
            //{
            //    ProjVM.Members.Clear();
            //}
            ProjVM.Members = new List<Student>();
            ProjVM.Members = projectContext.getMembers(id);
            ProjVM.Leader = projectContext.getLeader(id);
            return View(ProjVM);
        }

        public ProjectViewModel MapToProjVM(Project project)
        {

            ProjectViewModel ProjVM = new ProjectViewModel
            {
                ProjectID = project.ProjectID,
                ProjectDescription = project.ProjectDescription,
                ProjectTitle = project.ProjectTitle,
                ProjectPoster = project.ProjectPoster,
                ProjectURL = project.ProjectURL,

            };

            return ProjVM;
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            return View();
        }


        // POST: Project/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Project project)
        {
            if (ModelState.IsValid)
            {
                var studentId = HttpContext.Session.GetString("StudentID");
                project.ProjectID = projectContext.AddProject(project, Convert.ToInt32(studentId));
                return RedirectToAction("Index");
            }
            else
            {
                return View(project);
            }
        }

        // GET: Project/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) //Query string parameter not provided
            {
                return RedirectToAction("Index");
            }

            Project project = projectContext.GetProjectDetails(id.Value);
            if (project == null)
            {
                return RedirectToAction("Index");
            }
            return View(project);
        }

        // POST: Project/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Project project)
        {
            if (ModelState.IsValid)
            {
                projectContext.Update(project);
                return RedirectToAction("Index");
            }

            //Input validation fails, return to the view
            //to display error message
            return View(project);
        }

        // GET: Project/Delete/5
        public ActionResult Delete(int? id)
        {

            //IN THIS METHOD THE ID IS SHOWN
            

            Project project = projectContext.GetProjectDetails(id.Value);
            
            if (project == null)
            {
                return RedirectToAction("Index");
            }

            HttpContext.Session.SetString("ProjectID", id.Value.ToString());

            return View(project);
        }

        // POST: Project/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Project project)
        {
            // IN THIS METHOD EVERYTHING IS NULL
            // So i will use session cookie
            project.ProjectID = Convert.ToInt32(HttpContext.Session.GetString("ProjectID"));
            projectContext.Delete(project.ProjectID);
            return RedirectToAction("Index");
        }

        public ActionResult UploadPhoto(int id)
        {
            Project project = projectContext.GetProjectDetails(id);
            ProjectViewModel projVM = MapToProjVM(project);
            return View(projVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadPhoto(ProjectViewModel projVM)
        {
            if (projVM.FileToUpload != null && projVM.FileToUpload.Length > 0)
            {
                try
                {
                    // Find the filename extension of the file to be uploaded.
                    string fileExt = Path.GetExtension(projVM.FileToUpload.FileName);

                    // Rename the uploaded file with the staff’s name.
                    string uploadedFile = projVM.ProjectTitle + fileExt;

                    // Get the complete path to the images folder in server
                    string savePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images", uploadedFile);

                    // Upload the file to server
                    using (var fileSteam = new FileStream(savePath, FileMode.Create))
                    {
                        await projVM.FileToUpload.CopyToAsync(fileSteam);
                    }

                    //projVM.FileToUpload

                    projVM.ProjectPoster = uploadedFile;
                    projectContext.Upload(uploadedFile, projVM.ProjectID);
                    ViewData["Message"] = "File uploaded successfully.";
                }
                catch (IOException)
                {
                    //File IO error, could be due to access rights denied
                    ViewData["Message"] = "File uploading fail!";
                }
                catch (Exception ex) //Other type of error
                {
                    ViewData["Message"] = ex.Message;
                }
            }
            return View(projVM);
        }

        private List<SelectListItem> GetAllStudents(int id)
        {
            List<SelectListItem> students = new List<SelectListItem>();

            List<Student> studentList = studentContext.GetAllStudent();
            Student leader = projectContext.getLeader(id);

            //students.Add(
            //    new SelectListItem
            //    {
            //        Value = leader.StudentID.ToString() + "Leader",
            //        Text = leader.StudentName,
            //        Disabled = true
            //    });

            foreach (Student s in studentList)
            {
                students.Add(
                new SelectListItem
                {
                    Value = s.StudentID.ToString(),
                    Text = s.StudentName
                });
            }

            for (int i = 0; i < students.Count; i++)
            {
                for (int x = 0; x < GetAllMembers(id).Count; x++)
                {
                    if (students[i].Value == GetAllMembers(id)[x].Value)
                    {
                        students[i].Disabled = true;
                    }
                    if (students[i].Value == leader.StudentID.ToString())
                    {
                        students[i].Text = leader.StudentName + " - Leader";
                        students[i].Disabled = true;
                    }
                }
            }

            return students;
        }

        public ActionResult AddnRemoveMembers(int id)
        {
            Project project = projectContext.GetProjectDetails(id);
            ProjectViewModel projVM = MapToProjVM(project);
            ViewData["studentList"] = GetAllStudents(id);
            projVM.Members = new List<Student>();
            return View(projVM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddnRemoveMembers(IFormCollection ifc, int id, ProjectViewModel projVm)
        {
            var members = ifc["Members"];

            

            foreach (var item in members)
            {
                var student = studentContext.GetStudentProfile(Convert.ToInt32(item));
                projectContext.addMembers(student.StudentID,id);
                
            }
            
            ViewData["studentList"] = GetAllStudents(id);

            return RedirectToAction("Details",new { id = id });
        }
        
        private List<SelectListItem> GetAllMembers(int id)
        {
            List<SelectListItem> members = new List<SelectListItem>();
            Student leader = projectContext.getLeader(id);
            members.Add(
               new SelectListItem
               {
                   Value = leader.StudentID.ToString(),
                   Text = leader.StudentName += " - Leader",
                   Disabled = true
               });

            List<Student> memberList = projectContext.getMembers(id);
            foreach (Student s in memberList)
            {
                members.Add(
                new SelectListItem
                {
                    Value = s.StudentID.ToString(),
                    Text = s.StudentName
                });
            }

            return members;
        }

        public ActionResult RemoveMembers(int id)
        {
            ViewData["memberList"] = GetAllMembers(id);
            Project project = projectContext.GetProjectDetails(id);
            ProjectViewModel projVM = MapToProjVM(project);
            //projVM.Members = new List<Student>();
            return View(projVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveMembers(IFormCollection ifc, int id)
        {
            var members = ifc["Members"];

            foreach (var item in members)
            {
                var student = studentContext.GetStudentProfile(Convert.ToInt32(item));
                projectContext.removeMembers(student.StudentID, id);

            }

            return RedirectToAction("Details", new { id = id });
        }
    }
}
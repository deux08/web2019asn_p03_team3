﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web2019asn_p03_team3.DAL;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.Controllers
{
    public class StudentController : Controller
    {
        private StudentDAL studentContext = new StudentDAL();
        private SkillsetDAL skillsetContext = new SkillsetDAL();
        public IActionResult Index()
        {
            List<Student> studentList = studentContext.GetAllStudent();
            return View(studentList);
        }

        public ActionResult acknowledge (int sId)
        {
            int studentId = Convert.ToInt32(HttpContext.Session.GetString("StudentID"));
            studentContext.Acknowledge(sId, studentId);
            TempData["acknowledge"] = "you have acknowledge";
            return RedirectToAction("Studentsuggestion", new { id =  studentId });
        }

        // suggestion student 
        private List<Suggestion> GetSuggestionslist(int id)
        {
        
            List<Suggestion> suggestionslist = studentContext.GetStudentSuggestions(id);
            return suggestionslist;
        }
        public ActionResult Studentsuggestion(int id)
        {
            
            Student student = studentContext.GetStudentProfile(id);
            StudentViewModel studentvm = MapToStudentVM(student);
            ViewData["Acknowledge"] = TempData["acknowledge"];
            return View(studentvm);
        }
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Studentsuggestion()
        //{
        //    if (ModelState.IsValid)
        //    {
        //        ViewData["message"] = "you have acknowledge!!";
        //    }
        //    return RedirectToAction("Index");
        //}


        // GET: Student/Edit
        // when user see the edit.cshtml 

        public ActionResult Edit(int? id)
        {
            if (id == null) //Query string parameter not provided
            {
                return RedirectToAction("Index");
            }

            Student student = studentContext.GetStudentProfile(id.Value);
            if (student == null)
            {
                return RedirectToAction("Index");
            }
            TempData["msg"] = "Please fill in the boxes";

            return View(MapToStudentVM(student));


        }

        //when user clicks on the save in edit.cshtml
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Student student)
        {
            studentContext.Update(student);
            return RedirectToAction("Index");
           
        }
        
        private List<SelectListItem> GetSkillLists()
        {
            List<SelectListItem> skills = new List<SelectListItem>();
            skills.Add(
            new SelectListItem
            {
                Value = "",
                Text = "--Select--"
            });
            List<SkillSet> skilllist = skillsetContext.getAllSkillSets();
            foreach (SkillSet skill in skilllist)
            {
                skills.Add(
                new SelectListItem
                {
                    Value = skill.SkillSetID.ToString(),
                    Text = skill.SkillSetName
                });
            }
            return skills;
        }
        private List<SelectListItem> GetLecturerLists()
        {
            List<SelectListItem> lecturers = new List<SelectListItem>();
            lecturers.Add(
            new SelectListItem
            {
                Value = "",
                Text = "--Select--"
            });
            List<Lecturer> lecturerlist = studentContext.getstudentlecturer();
            foreach (Lecturer lect in lecturerlist)
            {
                lecturers.Add(
                new SelectListItem
                {
                    Value = lect.LecturerID.ToString(),
                    Text = lect.LecturerName
                });
            }
            return lecturers;
        }

        public ActionResult StudentSkill(int id)
        {
            ViewData["Skilllist"] = GetSkillLists();
            Student student = studentContext.GetStudentProfile(id);
            StudentViewModel studentvm = MapToStudentVM(student);
            return View(studentvm);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StudentSkill(StudentViewModel student)
        {
            ViewData["Skilllist"] = GetSkillLists();
            //student.StudentID = Convert.ToInt32(HttpContext.Session.GetString("StudentID"));
            if (ModelState.IsValid)
            {
                //Update staff record to database
                studentContext.updateskills(student);
                return RedirectToAction("Index");
            }
            //Input validation fails, return to the view
            //to display error message
            return View((student));
            //studentContext.Update(student);
            //return RedirectToAction("Index");
        }


            public ActionResult Studentphoto(int id)
        {
            Student student = studentContext.GetStudentProfile(id);
            StudentViewModel studentvm = MapToStudentVM(student);
            return View(studentvm);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Studentphoto(StudentViewModel studentvm)
        {
            if (studentvm.FileToUpload != null && studentvm.FileToUpload.Length > 0)
            {
                try
                {
                    // Find the filename extension of the file to be uploaded.
                    string fileExt = Path.GetExtension(studentvm.FileToUpload.FileName);

                    // Rename the uploaded file with the staff’s name.
                    string uploadedFile = studentvm.StudentID + fileExt;

                    // Get the complete path to the images folder in server
                    string savePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\images", uploadedFile);

                    // Upload the file to server
                    using (var fileSteam = new FileStream(savePath, FileMode.Create))
                    {
                        await studentvm.FileToUpload.CopyToAsync(fileSteam);
                    }
                    studentvm.StudentPhoto = uploadedFile;
                    studentContext.Upload(uploadedFile, studentvm.StudentID);
                    ViewData["Message"] = "File uploaded successfully.";
                }
                catch (IOException)
                {
                    //File IO error, could be due to access rights denied
                    ViewData["Message"] = "File uploading fail!";
                }
                catch (Exception ex) //Other type of error
                {
                    ViewData["Message"] = ex.Message;
                }
            }
            return View(studentvm);
        }

        

        // GET: Student/Profile 
        public ActionResult Profile(int id)
        {
            Student student = studentContext.GetStudentProfile(id);
            StudentViewModel StudentVM = MapToStudentVM(student);

            return View(StudentVM);
        }
        public StudentViewModel MapToStudentVM(Student student)
        {
            List<SkillSet> studentskillAlllist = skillsetContext.getAllSkillSets();
            List<Suggestion> suggestionslist = studentContext.GetStudentSuggestions(student.StudentID);

            StudentViewModel StudentVM = new StudentViewModel
            {
                StudentID = student.StudentID,
                StudentDescription = student.StudentDescription,
                StudentName = student.StudentName,
                StudentPhoto = student.StudentPhoto,
                StudentLink = student.StudentLink,
                StudentAchievement = student.StudentAchievement,
                StudentEmailAddr = student.StudentEmailAddr,
                MentorID = student.MentorID.ToString(),
                StudentCourse = student.StudentCourse,
                AllSkillSets = studentskillAlllist,
                Studentskills = skillsetContext.GetskillsetStudent(student.StudentID),
                StudentSuggestions = suggestionslist

            };

            return StudentVM;
        }




    }
}
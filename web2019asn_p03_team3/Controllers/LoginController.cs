﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using web2019asn_p03_team3.DAL;

namespace web2019asn_p03_team3.Controllers
{
    public class LoginController : Controller
    {
        LoginDAL LoginContext = new LoginDAL();

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult LecturerLogin()
        {
            return View();
        }

        public ActionResult StudentLogin()
        {
            return View();
        }

        public ActionResult LecturerMain()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LecturerLogin(IFormCollection formData)
        {   
            // Read inputs from textboxes            
            // Email address converted to lowercase             
            string loginID = formData["txtLoginID"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();

            var checkDetails = LoginContext.GetLecturer(loginID, password);

            if (checkDetails != null)
            {
                HttpContext.Session.SetString("LoginID", loginID);
                HttpContext.Session.SetString("LecturerID", checkDetails.LecturerID.ToString());
                HttpContext.Session.SetString("LecturerName", checkDetails.LecturerName.ToString());
                HttpContext.Session.SetString("Role", "Lecturer");

                return Redirect("~/Home/Index");
            }
            else
            {
                TempData["Message"] = "Invalid Login Credentials!";
                return Redirect("~/Login/LecturerLogin");
            }

        }

        [HttpPost]
        public ActionResult StudentLogin(IFormCollection formData)
        {   // Read inputs from textboxes            
            // Email address converted to lowercase             
            string loginID = formData["txtEmail"].ToString().ToLower();
            string password = formData["txtPassword"].ToString();

            var checkDetails = LoginContext.GetStudent(loginID, password);

            if (checkDetails != null)
            {
                HttpContext.Session.SetString("StudentID", checkDetails.StudentID.ToString());      
                HttpContext.Session.SetString("StudentName", checkDetails.StudentName.ToString());
                HttpContext.Session.SetString("Role", "Student");

                return Redirect("~/Home/Index");
            }
            else
            {
                TempData["Message"] = "Invalid Login Credentials!"; 
                return Redirect("~/Login/StudentLogin");
            }
        }

        public ActionResult LogOut()
        {
            // Clear all key-values pairs stored in session state
            HttpContext.Session.Clear();
            // Call the Index action of Home controller    
            return Redirect("~/Home/Index");
        }

        public ActionResult Create()
        {
            return View();
        }


    }
}
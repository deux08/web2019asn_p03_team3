﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using web2019asn_p03_team3.DAL;
using web2019asn_p03_team3.Models;

namespace web2019asn_p03_team3.Controllers
{
    public class SkillSetController : Controller
    {
        private SkillsetDAL ssContext = new SkillsetDAL();
        private StudentDAL sContext = new StudentDAL();
        private LecturerDAL lContext = new LecturerDAL();

        // GET: SkillSet
        public ActionResult Index()
        {
            if ((HttpContext.Session.GetString("Role") != "Lecturer"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<SkillSet> ssList = ssContext.getAllSkillSets();
            return View(ssList);
        }

        // GET: SkillSet/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
            
        // GET: SkillSet/Create
        public ActionResult Create()
        {
            if (HttpContext.Session.GetString("Role") == null || HttpContext.Session.GetString("Role")  != "Lecturer")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        // POST: SkillSet/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SkillSet ss)
        {
            if (ssContext.isSkillSetExists(ss.SkillSetName))
            {
                ss.SkillSetID = ssContext.AddSkillSet(ss);
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["Message"] = "Skillset name already exists!";
                return View();
            }
        }

        public ProjectViewModel MapToSSVM(SkillSet ss)
        {
            ProjectViewModel SSVM = new ProjectViewModel
            {
                ProjectID = ss.SkillSetID,
                ProjectTitle = ss.SkillSetName
            };

            return SSVM;
        }

        // GET: SkillSet/Edit/5
        public ActionResult Edit(int? id)
        {

            SkillSet ss = ssContext.GetSkillSetDetails(id.Value);
            if (ss == null)
            {
                return RedirectToAction("Index");
            }

            HttpContext.Session.SetString("SkillSetID", id.Value.ToString());

            return View(ss);
        }

        // POST: SkillSet/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SkillSet ss)
        {
            ss.SkillSetID = Convert.ToInt32(HttpContext.Session.GetString("SkillSetID"));
            ssContext.Update(ss);
            return RedirectToAction("Index");
        }

        // GET: SkillSet/Delete/5
        public ActionResult Delete(int? id)
        {
            SkillSet ss = ssContext.GetSkillSetDetails(id.Value);

            if (ss == null)
            {
                return RedirectToAction("Index");
            }

            HttpContext.Session.SetString("SkillSetID", id.Value.ToString());
            return View();
        }

        // POST: SkillSet/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(SkillSet ss)
        {
            ss.SkillSetID = Convert.ToInt32(HttpContext.Session.GetString("SkillSetID"));
            if (ssContext.isStudentSkillSetExists(ss.SkillSetID))
            {
                ssContext.Delete(ss.SkillSetID);
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["Message"] = "Skillset is assigned to some students!";
                return View();
            }
        }

        public List<Student> GetStudents(int id)
        {
            List<int> studentID = ssContext.GetStudentID(id);
            List<Student> studentSkillSet = new List<Student>();

            foreach(int s in studentID)
            {
                studentSkillSet.Add(sContext.GetStudentProfile(s));
            }
            return studentSkillSet;
        }

        public ActionResult ViewStudent(int id)
        {
            SkillSet ss = ssContext.GetDetails(id);
            List<Student> studentList = GetStudents(id);
            StudentSkillSet studentSkillSet = new StudentSkillSet { SkillSet = ss, Students = studentList };
            return View(studentSkillSet);
        }

        public ActionResult ViewProfile(int id)
        {
            MenteeViewModel m = new MenteeViewModel { skillset = lContext.GetSkillSets(id), student = lContext.GetMentee(id) };
            return View(m);
        }
    }
}